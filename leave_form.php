<?php

session_start();

$newstr=$_GET['id'];
$len=strlen($newstr);
$oldStr="";
$offset=7;

for($i=0; $i<$len; $i=$i+1)
  {
   $a=ord($newstr[$i])-$offset;

   $oldStr = $oldStr.chr($a);
  } 
$oldStr="10BCE1014";
$regnumber=$oldStr;
$_SESSION['user']=$regnumber;
$_SESSION['url']=$_GET['id'];
require_once 'getdetails.php';
$sql="select * from register where registerno='$regnumber'";
$result=mysqli_query($con,$sql)or die(mysqli_error($con));
$r=mysqli_fetch_array($result);
if($r['block']==0)
  echo "You are Blocked by the Admin. Meet the Admin to solve the issue";
else
{
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Leave Form Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<link href="css/bootstrap-combined.min.css" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link type="text/css" href="css/bootstrap.min.css" />
        <link type="text/css" href="css/bootstrap-timepicker.min.css" />
        <script type="text/javascript" src="jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-2.2.2.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-timepicker.min.js"></script>
	<style type="text/css">

.ds_box {
	background-color: #FFF;
	border: 1px solid #000;
	position: absolute;
	z-index: 32767;
}

.ds_tbl {
	background-color: #FFF;
}

.ds_head {
	background-color: #333;
	color: #FFF;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	text-align: center;
	letter-spacing: 2px;
}

.ds_subhead {
	background-color: #CCC;
	color: #000;
	font-size: 12px;
	font-weight: bold;
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	width: 32px;
}

.ds_cell {
	background-color: #EEE;
	color: #000;
	font-size: 13px;
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	padding: 5px;
	cursor: pointer;
}

.ds_cell:hover {
	background-color: #F3F3F3;
} /* This hover code won't work for IE */

</style>
</head>
<body>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
<tr><td id="ds_calclass">
</td></tr>
</table>
<script src="datefile.js"></script>

    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
  <body>
<?php
include  'header.php';
?>
  
  
<div class="row-fluid">      <div class="span1"></div>      <div class="span10">
<table class="table table-bordered" align="center"style="background-color:#E8E8E8">
<form class="form-horizontal" method="post" action='leave_form1.php?type=leave' align="center">

<th colspan="4"><center>LEAVE FORM</center></th> 
<tr>
	<td><b>Name</b></td><td><label><?php echo $row['name'];?></label></td>
	<td><b>Reg No.</b></td><td><label><?php echo $row['registerno'];?></label></td>
</tr>
<tr>
	<td><b>Block</b></td><td><?php echo $row['blockname'];?></td>
	<td><b>Room No.</b></td><td><?php echo $row['roomno'];?></td>
</tr>
<tr>
	<td class="table_left" ><b>Date and Time of leaving:</b></td><td>
	<div id="datetimepicker2" class="input-append date">
		
      <input id="from_date" name="from_date" class="input-small" data-format="yyyy-MM-dd" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker2').datetimepicker({
      language: 'en',
	  pickTime:false
       
    });
  });
</script>
	<div id="datetimepicker3" class="input-append date">
		
      <input id="from_hours" name="from_hours"class="input-small"data-format="hh:mm:ss" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      language: 'en',
       pickDate:false
    });
  });
</script>
	<td class="table_left" ><b>Date and Time of arrival</b></td><td>	
	<div id="datetimepicker4" class="input-append date">
		
      <input id="to_date" name="to_date" class="input-small"data-format="yyyy-MM-dd" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      language: 'en',
       pickTime:false
    });
  });
</script>
		<div id="datetimepicker5" class="input-append date">
		
      <input id="to_hours" name="to_hours" class="input-small" data-format="hh:mm:ss" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker5').datetimepicker({
      language: 'en',
       pickDate:false
    });
  });
</script>
	
	
	</tr>


<tr>
	<td><b>Permanent Address:</b></td><td><?php echo $row['perm']?></td>
	<td><b>Visiting Address:</b></td><td><textarea id="to_address" name="to_address" type="text" cols="25" rows="4" required></textarea></td>
</tr>
<tr>
	<td><b>Student Mobile No.</b></td><td><label><?php echo $row['studentmobile'];?></label></td>
	<td><b>Father Mobile No.</b></td><td><label><?php echo $row['parentmobileno'];?></label></td>
</tr>
<tr>
<td colspan="4" class="text-error"><center>
<?php
if(isset($_GET['message']))
{
$msg=$_GET['message'];
echo $msg;
}
?>
<tr><th colspan="4"><center>MESS</center></th> 

<tr>
	<td colspan="9" ><center><input class="btn btn-primary" type="submit" value="SUBMIT LEAVE FORM" name="SubmitForm"></td>
</tr>

</table>

</form>
</div>
</div>
<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php
}?>