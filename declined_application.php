<?php
	session_start();
	
	require_once 'credentials.php';
	$user=$_SESSION['user'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Leave Form Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 60px;
      }

      /* Custom container */
      .container {
        margin: 0 auto;
        max-width: 1000px;
      }
      .container > hr {
        margin: 60px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 80px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 100px;
        line-height: 1;
      }
      .jumbotron .lead {
        font-size: 24px;
        line-height: 1.25;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }


      /* Customize the navbar links to be fill the entire space of the .navbar */
      .navbar .navbar-inner {
        padding: 0;
      }
      .navbar .nav {
        margin: 0;
        display: table;
        width: 100%;
      }
      .navbar .nav li {
        display: table-cell;
        width: 1%;
        float: none;
      }
      .navbar .nav li a {
        font-weight: bold;
        text-align: center;
        border-left: 1px solid rgba(255,255,255,.75);
        border-right: 1px solid rgba(0,0,0,.1);
      }
      .navbar .nav li:first-child a {
        border-left: 0;
        border-radius: 3px 0 0 3px;
      }
      .navbar .nav li:last-child a {
        border-right: 0;
        border-radius: 0 3px 3px 0;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
			$today = date("Y-m-d");
if($_SESSION['role']=='P')
{
include  'proctor_header.php';
}
if($_SESSION['role']=='M')
{
include  'warden_header.php';
}

include 'timefile.php';
?>
  <body>

    <div class="container">

      <div class="masthead">
        </div>
<form name="form1" method="POST" action="warden_write.php?id=d">
		<table class="table table-bordered">
		<tr><th>Name<th>Registration number<th>Block<th>Room Number<Th>Out Time<th>In Time<th>Visiting Address<th>Student Mobile<th>Father's Mobile<th>
		<?php
		require_once 'credentials.php';
	if($_SESSION['role']=='P')
{
$user1=$_SESSION['user'];
$sql2="Select * from reports where proc_id='$user'" ;
$res2=mysqli_query($con,$sql2)or die("cannot get");
while($r2=mysqli_fetch_array($res2))
{
$proctee_name=$r2['regno'];

$sql1="Select * from leave_form where status='Declined' and fromdate>='$today' and regno='$proctee_name' order by fromdate,outtime asc" ;
$res=mysqli_query($con,$sql1)or die("cannot get1");

while($r=mysqli_fetch_array($res))
{
if($r['fromdate']!=$r['todate'])
{
ECHO "<TR class='error'>";
}
else if($r['fromdate']==$r['todate'])
{
ECHO "<TR class='success'> ";
}
if(strtotime($r['intime'])>$admintime)
{
ECHO "<TR class='error'>";
}
if(strtotime($r['intime'])<strtotime($r['outtime']))
{
ECHO "<TR class='error'>";
}
echo "<TD>".htmlentities($r['name']);
echo "<TD>".htmlentities($r['regno']);

echo "<TD>".htmlentities($r['block']);
echo "<TD>".htmlentities($r['roomno']);

echo "<TD>".htmlentities($r['fromdate'])." ".htmlentities($r['outtime']);
echo "<TD>".htmlentities($r['todate'])." ".htmlentities($r['intime']);

echo "<TD>".htmlentities($r['visiting_address']);
echo "<TD>".htmlentities($r['student_mobile']);
echo "<TD>".htmlentities($r['father_mobile']);
echo "<TD>";
?>
<input type="checkbox" name="total[]" class="checkbox" value="<?php echo htmlentities($r['id']);  ?>" id="<?php echo htmlentities($r['id']); ?>" >
<td>
<select name="<?php echo htmlentities($r['id']);  ?>">
<option  value="Talked to parents">
Talked to parents
</option>
<option  value="At my risk">
At my risk
</option>
</select>

<?php
}



}
}
else
{
$sql1="Select * from leave_form where status='Declined' and fromdate>='$today' order by fromdate,outtime asc" ;
$res=mysqli_query($con,$sql1)or die("cannot get");
while($r=mysqli_fetch_array($res))
{
if($r['fromdate']!=$r['todate'])
{
ECHO "<TR class='error'>";
}
else if($r['fromdate']==$r['todate'])
{
ECHO "<TR class='success'> ";
}
if(strtotime($r['intime'])>strtotime($admintime))
{
ECHO "<TR class='error'>";
}
if(strtotime($r['intime'])<strtotime($r['outtime']))
{
ECHO "<TR class='error'>";
}
echo "<TD>".htmlentities($r['name']);
echo "<TD>".htmlentities($r['regno']);

echo "<TD>".htmlentities($r['block']);
echo "<TD>".htmlentities($r['roomno']);

echo "<TD>".htmlentities($r['fromdate'])." ".htmlentities($r['outtime']);
echo "<TD>".htmlentities($r['todate'])." ".htmlentities($r['intime']);

echo "<TD>".htmlentities($r['visiting_address']);
echo "<TD>".htmlentities($r['student_mobile']);
echo "<TD>".htmlentities($r['father_mobile']);
echo "<TD>";

?>
		
		<input type="checkbox" name="total[]" class="checkbox" value="<?php echo htmlentities($r['id']);  ?>" id="<?php echo htmlentities($r['id']); ?>" >
<?php
}
}
?><tr><td colspan="10">
<center>
		<input type="submit" name="submit" value="Pending" class="btn btn-primary ">
		<input type="submit" name="submit" value="Approve" class="btn btn-primary ">
		<input type="submit" name="submit" value="Decline" class="btn btn-primary disabled"disabled>
		</center>
		
		
		</table>
		
		
      <!-- Jumbotron -->
      
      <!-- Example row of columns -->
      
      
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php

?>