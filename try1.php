<head>
 <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
 <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
 <link href="css/bootstrap-combined.min.css" rel="stylesheet">
    
  </head>
  <body>
    <div id="datetimepicker3" class="input-append date">
      <input id="datetimepicker3" data-format="hh:mm:ss" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      language: 'en',
      pickDate:false
    });
  });
</script>
  </body>
<html>