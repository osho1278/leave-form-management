<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Registration Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>

  <body style="background-color:#2B3B6C">
<div class="row-fluid"><div class="span4"></div>
      <div class="span4">
<form class="form-horizontal"  method="post" align="center" action="register2.php" enctype="multipart/form-data">
<table class="table table-bordered" style="background-color:#ffffff" >
<th colspan="2" text size="3"><center>REGISTER</center></th>
<tr><td >Name</td><td><input type="text" name="student_name"></input></td></tr>
<tr><td>Register No.</td><td><input type="text" name="student_reg_no" maxlength="9"></input></td></tr>
<tr><td>Image</td><td><input name="img" type="file"></input></td></tr>
<tr><td>Password</td><td><input type="password" name="password" title="Password is case sensitive"></input></td></tr>
<tr><td>Re-enter Password</td><td><input type="password" name="re-password" title="Password is case-sensitive"></input></td></tr>
<tr><td>Student Mobile No.</td><td><input type="tel" pattern="[0-9]{10}" name="student_mobile" maxlength="10"></input></td></tr>
<tr><td>Room No.</td><td><input type="text" name="room_no" maxlength="5"></input></td></tr>
<tr><td>Block Name.</td><td><input type="text" name="block" ></input></td></tr>
<tr><td>Parent's Mobile No.</td><td><input type="tel" pattern="[0-9]{10}" name="parent_mobile" maxlength="10"></input></td></tr>
<tr><td>Permanent Address:</td><td><textarea id="from_address" name="from_address" type="text" cols="25" rows="4" required></textarea></td></tr>
<tr><td>Email ID</td><td><input type="email" name="email"></td></tr>
<tr><td>Proctor's Name</td><td><input type="text" name="proctor"></input></td></tr>
<tr><td colspan="4"><center><input type="submit" class="btn btn-primary" value="submit" name="submit" ></tr>
</table>

</form>
</div>
</div>
<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
