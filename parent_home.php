<?php
session_start();
$newstr=$_GET['id'];
$len=strlen($newstr);
$oldStr="";
$offset=7;

for($i=0; $i<$len; $i=$i+1)
  {
   $a=ord($newstr[$i])-$offset;

   $oldStr = $oldStr.chr($a);
  } 
 // $oldStr="10BCE1014";
$regnumber=$oldStr;
$_SESSION['user']=$regnumber;
$_SESSION['url']=$_GET['id'];
$_SESSION['role']='parent'
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Leave Form Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
include  'parent_header.php';
?>

  <body>
 <div class="container">
<form class="form-horizontal" method="post" align="center">
<table class="table table-bordered" >
</table>

</form>
      <div class="masthead">
        <form class="form-horizontal" method="post" align="center">
<table class="table table-bordered" >
<tr><th>Name<th>Registration number<th>Block<th>Room Number<Th>Out Time<Th>Outtime(Actual)<th>In Time<Th>Intime(actual)<th>Visiting Address<th>Student Mobile<th>Father's Mobile<th>Status
		
<?php

require_once "credentials.php";
$no=$_SESSION['user'];

$sql1="select * from leave_form where regno='$no' order by fromdate" ;
$res=mysqli_query($con,$sql1)or die("cannot get");
while($r=mysqli_fetch_array($res))
{
if($r['status']=="Approved")
{

			$today = date("Y-m-d");

$time = date('h:i:s', time());
//echo $date;
//if($r['todate']<=$today && $time > $r['intime'] )

if($r['todate']<$today)
{
ECHO "<TR style='background-color:orange;'>";
//echo "late";
}
else if ($r['todate']==$today && $time > $r['intime'] )
{
ECHO "<TR style='background-color:orange;'>";
}
else
{
ECHO "<TR class='success'>";
}

//ECHO "<TR class='success'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];
echo "<TD>".$r['fromdate']."<br>".$r['outtime'];
echo "<TD>".$r['gate_out'];
echo "<TD>".$r['todate']."<br>".$r['intime'];
echo "<TD>".$r['gate_in'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
}
if($r['status']=="Pending")
{
ECHO "<TR class='info'>";

echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];
echo "<TD>".$r['fromdate']."<br>".$r['outtime'];
echo "<TD>".$r['gate_out'];
echo "<TD>".$r['todate']."<br>".$r['intime'];
echo "<TD>".$r['gate_in'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
}
if($r['status']=="Declined")
{
ECHO "<TR class='error'>";
echo "<TD>".$r['name'];
echo "<TD>".$r['regno'];

echo "<TD>".$r['block'];
echo "<TD>".$r['roomno'];
echo "<TD>".$r['fromdate']."<br>".$r['outtime'];
echo "<TD>".$r['gate_out'];
echo "<TD>".$r['todate']."<br>".$r['intime'];
echo "<TD>".$r['gate_in'];
echo "<TD>".$r['visiting_address'];
echo "<TD>".$r['student_mobile'];
echo "<TD>".$r['father_mobile'];
echo "<TD>".$r['status'];
}


}

?>




</table>

</form>
</div>
</div>
<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php


?>