<?php
session_start();
require_once 'sessiontimout.php';
if(isset($_SESSION['user'])&& $_SESSION['role']==='A')
{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>View Status</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
include  'admin_header.php';
?>

  <body>
 <div class="container">

      <div class="masthead">
        <form class="form-horizontal" method="post" align="center">
<table class="table table-bordered" >
<tr><th>Registration number<th>Name<th>Block<th>Room Number<Th>Student Number<th>Parents Number<th>Email Address<th>Proctor's Name<th>Default Times
		
<?php
require_once 'credentials.php';
$sql1="select distinct(regno) from gate" ;
$res=mysqli_query($con,$sql1)or die("cannot get");
while($r=mysqli_fetch_array($res))
{
$count=0;
$regn=$r['regno'];
$sql2="select * from gate where regno='$regn'" ;
$res2=mysqli_query($con,$sql2)or die("cannot get");
while($r2=mysqli_fetch_array($res2))
{
$count=$count+1;
}

if($count>1)
{
$sql3="select * from register where registerno='$regn'" ;
$res3=mysqli_query($con,$sql3)or die("cannot get");
$r3=mysqli_fetch_array($res3);
ECHO "<TR class='error'>";

echo "<TD>".$r3['registerno'];
echo "<TD>".$r3['name'];

echo "<TD>".$r3['blockname'];
echo "<TD>".$r3['roomno'];
echo "<TD>".$r3['studentmobile'];
echo "<TD>".$r3['parentmobileno'];
echo "<TD>".$r3['emailid'];
echo "<TD>".$r3['proctorsname'];


}
else
{
$sql3="select * from register where registerno='$regn'" ;
$res3=mysqli_query($con,$sql3)or die("cannot get");
$r3=mysqli_fetch_array($res3);
ECHO "<TR class='success'>";

echo "<TD>".$r3['registerno'];
echo "<TD>".$r3['name'];

echo "<TD>".$r3['blockname'];
echo "<TD>".$r3['roomno'];
echo "<TD>".$r3['studentmobile'];
echo "<TD>".$r3['parentmobileno'];
echo "<TD>".$r3['emailid'];
echo "<TD>".$r3['proctorsname'];

}
echo "<TD>".$count;

}
?>




</table>

</form>
</div>
</div>
<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'index.php?id=kindly login before continuing';</script>";

?>

