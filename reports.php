<?php
session_start();
require_once 'sessiontimout.php';
if(isset($_SESSION['user'])&& $_SESSION['role']==='A')
{

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Leave Form Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="ico/favicon.png">
  </head>
<?php
include  'admin_header.php';
?>

  <body>
<div class="row-fluid"><div class="span3"></div>
      <div class="span6">
<form class="form-horizontal" method="post" align="center" action="reports.php?id=get">
From&nbsp;&nbsp;&nbsp;&nbsp;<div id="datetimepicker2" class="input-append date">
   
      <input id="from_date" name="from_date" class="input-small" data-format="yyyy-MM-dd" type="text"></input>
      
      
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker2').datetimepicker({
      language: 'en',
    pickTime:false
       
    });
  });
</script> &nbsp;&nbsp;&nbsp;&nbsp;to &nbsp;&nbsp;&nbsp;&nbsp;
<div id="datetimepicker4" class="input-append date">
    
      <input id="to_date" name="to_date" class="input-small" data-format="yyyy-MM-dd" type="text"></input>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="js/jquery.min.js">
    </script> 
    <script type="text/javascript"
     src="js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
     src="js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      language: 'en',
       pickTime:false
    });
  });
</script>
<input type="submit" class="input_small">

</form>
</div>
</div>

<div class="row-fluid">      <div class="span2">
</div>
      <div class="span8">
      <center>

<?php

if(isset($_GET['id']))
{

require_once 'credentials.php';
$from=mysqli_real_escape_string($con,$_POST['from_date']);
$to=mysqli_real_escape_string($con,$_POST['to_date']);

Echo "Showing data from <b>".$from ." to ".$to."</b>";

?>
<br><br>

<table class="table">
<th>Registration No.<th>Name<th>Block<th>Room No.<th>Permanent Address<th>Visiting Address<th>Student Phone<th>Parents Phone
<?php

//echo $to;
$sql="select * from leave_form where gate_in >'$to' and gateout< '$from'";
$res=mysqli_query($con,$sql)or die(mysqli_error($con));
while($r=mysqli_fetch_array($res))
{


?>
<tr>
<Td><?php echo $r['regno']; ?>
<Td><?php echo $r['name']; ?>
<Td><?php echo $r['block']; ?>
<Td><?php echo $r['roomno']; ?>
<Td><?php echo $r['permanent_address']; ?>
<Td><?php echo $r['visiting_address']; ?>
<Td><?php echo $r['student_mobile']; ?>
<Td><?php echo $r['father_mobile']; ?>

<?php
}
}

?>



</Table>
</div>
</div>


<div id="footer">
</div>
<script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>

  </body>
</html>
<?php
}
else
echo "<script type="."text/javascript".">location.href = 'index.php?id=kindly login before continuing';</script>";

?>
